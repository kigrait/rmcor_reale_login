package com.rmcor.reale.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping()
public class HealthCheckupController {

	@RequestMapping(value="health_checkup" ,method=RequestMethod.GET)
	public String test() {
		return "Project is working fine";
	}
}
