package com.rmcor.reale.controller;
/**
*
* @author Shailendra yadav
*/
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.rmcor.reale.model.LoginModel;
import com.rmcor.reale.model.RegistrationModel;
import com.rmcor.reale.service.UserService;
import com.rmcor.reale.utilites.ResponseModel;

@RestController
@RequestMapping(value = "user")
public class UserController {

	@Autowired
	public UserService userService;

	/* Service for user Registration */
	@RequestMapping(value = "/registration", method = RequestMethod.POST)
	public ResponseModel saveUserRegistration(@RequestBody RegistrationModel registrationModel) {
		return userService.saveUserRegistration(registrationModel);
	}
	
	/* Service for user Login */
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public ResponseModel userLogin(@RequestBody LoginModel loginModel) {
		return userService.userLogin(loginModel);
	}
}
