package com.rmcor.reale.controller;

import static org.springframework.http.ResponseEntity.ok;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rmcor.reale.service.UploadService;
import com.rmcor.reale.utilites.ResponseModel;
import com.sun.istack.NotNull;

@RestController
@RequestMapping("/upload")
public class UploadController {
	


	@Autowired
	private UploadService uploadService;
	
	/* Method for testing */
	@RequestMapping(value="/test",method=RequestMethod.GET)
	public String test() {
		return "upload class working fine";
	}
	
	/* Method for Upload Image Video & Documents */
	@RequestMapping(value="/file_upload",method=RequestMethod.POST)
	public ResponseModel upload(@RequestParam("file") MultipartFile file) {
		return uploadService.callUpload(file);
	}
	
	 @PostMapping(
	            value = "/file_upload_demo",
	            consumes = {
	                    MediaType.MULTIPART_FORM_DATA_VALUE,
	                    MediaType.APPLICATION_OCTET_STREAM_VALUE})
	    public ResponseEntity<?> createVideo(
	            @RequestPart("content")  @NotNull  MultipartFile file) throws IOException {
	        String contentType = file.getContentType();
	        System.out.println("contentType "+contentType);
	        String metaData = uploadService.createFile(file);
	        return ok(metaData);
	    }
}
