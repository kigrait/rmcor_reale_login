package com.rmcor.reale.Impl.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.rmcor.reale.dao.UploadDao;
import com.rmcor.reale.service.UploadService;
import com.rmcor.reale.utilites.Constants;
import com.rmcor.reale.utilites.FileStorageUtil;
import com.rmcor.reale.utilites.ResponseModel;

@Service
@Transactional
public class UploadServiceImpl implements UploadService {

	@Autowired
	private UploadDao uploadDao;

	@Autowired
	private FileStorageUtil storageUtil;

	/* Method for Upload Image Video & Documents */
	@Override
	public ResponseModel callUpload(MultipartFile file) {
		ResponseModel res = new ResponseModel();
		if (file.isEmpty()) {
			res.setMessage(Constants.SELECT_FILE);
			res.setStatus(Constants.ERRORCODE);
			return res;
		}
		try {
			String fileExtension = StringUtils.getFilenameExtension(file.getOriginalFilename());
			String fileName = StringUtils.getFilename(file.getOriginalFilename());
			System.out.println("fileName" + fileName);
			System.out.println("extension = " + fileExtension);
			res.setMessage(Constants.UPLOADED + " " + file.getOriginalFilename());
			res.setStatus(Constants.SUCCESSCODE);
			/* Code for Upload Image */
			if (Arrays.asList(Constants.IMAGE_EXTENSIONS).contains("." + fileExtension)) {
				System.out.println("extension image = " + fileExtension);
				/* Get the file and save it somewhere */
				byte[] bytes = file.getBytes();
				Path path = Paths.get(Constants.IMAGE_UPLOADED_FOLDER + System.currentTimeMillis() + "_"
						+ file.getOriginalFilename());
				Files.write(path, bytes);
			}
			/* Code for Upload Video */
			else if (Arrays.asList(Constants.VIDEO_EXTENSIONS).contains("." + fileExtension)) {
				System.out.println("extension video = " + fileExtension);
				/* Get the file and save it somewhere */
				byte[] bytes = file.getBytes();
				Path path = Paths.get(Constants.VIDEO_UPLOADED_FOLDER + System.currentTimeMillis() + "_"
						+ file.getOriginalFilename());
				Files.write(path, bytes);
			}
			/* Code for Upload Documents */
			else if (Arrays.asList(Constants.DOCUMENT_EXTENSIONS).contains("." + fileExtension)) {
				System.out.println("extension document = " + fileExtension);
				/* Get the file and save it somewhere */
				byte[] bytes = file.getBytes();
				Path path = Paths.get(Constants.DOCUMENT_UPLOADED_FOLDER + System.currentTimeMillis() + "_"
						+ file.getOriginalFilename());
				Files.write(path, bytes);
			}else {
				res.setMessage(Constants.NOT_UPLOADED + " " + file.getOriginalFilename());
				res.setStatus(Constants.ERRORCODE);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	@Override
	public String createFile(MultipartFile file) {
		try {
			String path = storageUtil.createFile(file);
			System.out.println("path  " + path);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
