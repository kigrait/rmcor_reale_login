package com.rmcor.reale.Impl.service;
/**
*
* @author Shailendra yadav
*/
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.rmcor.reale.dao.UserDao;
import com.rmcor.reale.hibernate.model.Login;
import com.rmcor.reale.hibernate.model.Registration;
import com.rmcor.reale.model.LoginModel;
import com.rmcor.reale.model.RegistrationModel;
import com.rmcor.reale.service.UserService;
import com.rmcor.reale.utilites.Constants;
import com.rmcor.reale.utilites.ResponseModel;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	public UserDao userDao;

	/* method for user registration in service */
	@Override
	public ResponseModel saveUserRegistration(RegistrationModel registrationModel) {
		String loginRes;
		Map<String, Object> res1 = new HashMap<String, Object>();
		ResponseModel res = new ResponseModel();
		Registration registration = new Registration();
		/* method to check mobile number exist or not */
		Integer mobileNumber = checkMobileNumber(registrationModel);
		if (mobileNumber != null && mobileNumber <= 0) {
			/* method to check email id exist or not */
			Integer emailId = checkEmailId(registrationModel);
			if (emailId != null && emailId <= 0) {
				registration = setRegistrationModelToPojo(registrationModel);
				registration = userDao.saveUserRegistration(registration);
				if (registration != null && registration.getPid() != null) {
					/* method to save detail in login table */
					Login login = new Login();
					login = setLoginModelToPojo(registration);
					loginRes = userDao.saveLoginDetail(login);
					res.setMessage(Constants.SUCCESS);
					res.setStatus(Constants.SUCCESSCODE);
					res1.put("data", "User completed a registration process successfully");
					if (loginRes.equalsIgnoreCase(Constants.ERRORCODE)) {
						res.setMessage(Constants.ERROR);
						res.setStatus(Constants.ERRORCODE);
						res1.put("data", "Error in entering the login detail");
					}
				} else {
					res.setMessage(Constants.Registration_Error);
					res.setStatus(Constants.ERRORCODE);
					res1.put("data", "Error in entering the registration detail");
				}
			} else {
				res.setMessage(Constants.Mobile_Number_Exist);
				res.setStatus(Constants.Already_Exist_Code);
				res1.put("data", "Email Id already exist");
			}
		} else {
			res.setMessage(Constants.Mobile_Number_Exist);
			res.setStatus(Constants.Already_Exist_Code);
			res1.put("data", "Mobile number already exist");
		}
		res.setResponse(res1);
		return res;
	}

	/* method to check email id exist or not */
	private Integer checkEmailId(RegistrationModel registrationModel) {
		return userDao.checkEmailId(registrationModel);
	}

	/* method to check mobile number exist or not */
	private Integer checkMobileNumber(RegistrationModel registrationModel) {
		return userDao.checkMobileNumber(registrationModel);
	}

	/* method to set login model to pojo */
	private Login setLoginModelToPojo(Registration source) {
		Login dest = new Login();
		dest.setUserName(source.getUserName());
		dest.setMobileNumber(source.getMobileNumber());
		dest.setEmailId(source.getEmailId());
		dest.setPassword(source.getPassword());
		dest.setDateTime(String.valueOf(System.currentTimeMillis()));
		dest.setRegisteredUserId(source.getPid());
		dest.setIsactive(1);
		return dest;
	}

	/* method to set registration model to pojo */
	private Registration setRegistrationModelToPojo(RegistrationModel source) {
		Registration dest = new Registration();
		dest.setFirstName(source.getFirstName());
		dest.setMiddleName(source.getMiddleName());
		dest.setLastName(source.getLastName());
		dest.setMobileNumber(source.getMobileNumber());
		dest.setEmailId(source.getEmailId());
		dest.setUserName(source.getFirstName());
		dest.setPassword(String.valueOf(ThreadLocalRandom.current().nextInt()));
		dest.setIsactive(1);
		dest.setDateTime(String.valueOf(System.currentTimeMillis()));
		return dest;
	}

	/* method for user login service */
	@Override
	public ResponseModel userLogin(LoginModel loginModel) {
		ResponseModel res = new ResponseModel();
		Map<String, Object> res1 = new HashMap<String, Object>();
		res.setMessage(Constants.ERROR);
		res.setStatus(Constants.ERRORCODE);
		Login loginResponse = userDao.userLogin(loginModel);
		System.out.print("value of loginResponse "+loginResponse.getPid());
		if(loginResponse!=null && loginResponse.getPid()!=null) {
			res.setMessage(Constants.SUCCESS);
			res.setStatus(Constants.SUCCESSCODE);
			res1.put("data",loginResponse.toString());
		}
		return res;
	}

}
