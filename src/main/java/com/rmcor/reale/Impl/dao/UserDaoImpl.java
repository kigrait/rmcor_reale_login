package com.rmcor.reale.Impl.dao;
/**
*
* @author Shailendra yadav
*/
import javax.transaction.Transactional;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.rmcor.reale.dao.UserDao;
import com.rmcor.reale.hibernate.model.Login;
import com.rmcor.reale.hibernate.model.Registration;
import com.rmcor.reale.model.LoginModel;
import com.rmcor.reale.model.RegistrationModel;
import com.rmcor.reale.utilites.Constants;
import com.rmcor.reale.utilites.ResponseModel;

@Repository("userDao")
@Transactional
public class UserDaoImpl implements UserDao {

	@Autowired
	private SessionFactory sessionFactory;

	private Session getSession() {
		Session session = sessionFactory.getCurrentSession();
		if (session == null) {
			session = sessionFactory.openSession();
		}
		return session;
	}

	/* method to insert user detail in registration table in DAO */
	@Override
	public Registration saveUserRegistration(Registration registration) {
		Long id = (Long) getSession().save(registration);
		return (Registration) getSession().get(Registration.class, id);

	}

	/* method to insert user detail in login table in DAO */
	@Override
	public String saveLoginDetail(Login login) {
		Long id = (Long) getSession().save(login);
		if (id >= 1) {
			return Constants.SUCCESSCODE;
		}
		return Constants.ERRORCODE;
	}

	/* method to check mobile exist in registration table in DAO */
	@Override
	public Integer checkMobileNumber(RegistrationModel registrationModel) {
		Criteria cr = getSession().createCriteria(Registration.class);
		cr.add(Restrictions.eq("mobileNumber", registrationModel.getMobileNumber()));
		return cr.list().size();
	}

	/* method to check mobile exist in registration table in DAO */
	@Override
	public Integer checkEmailId(RegistrationModel registrationModel) {
		Criteria cr = getSession().createCriteria(Registration.class);
		cr.add(Restrictions.eq("emailId", registrationModel.getEmailId()));
		return cr.list().size();
	}

	/* method for user login in login table in DAO */
	@Override
	public Login userLogin(LoginModel loginModel) {
		Login login = new Login();
		Criteria cr = getSession().createCriteria(Login.class);
		if(loginModel!=null && loginModel.getMobileNumber()!=null) {
			cr.add(Restrictions.eq("mobileNumber", loginModel.getMobileNumber()));
			cr.add(Restrictions.eq("password", loginModel.getPassword()));
			cr.add(Restrictions.eq("isactive",1));
		}else if(loginModel!=null && loginModel.getEmailId()!=null) {
			cr.add(Restrictions.eq("emailId", loginModel.getEmailId()));
			cr.add(Restrictions.eq("password", loginModel.getPassword()));
			cr.add(Restrictions.eq("isactive", 1));
		}
		if(cr.list().size()>0) {
			login = (Login) cr.list().get(0);
		}
		return login;
	}

}
