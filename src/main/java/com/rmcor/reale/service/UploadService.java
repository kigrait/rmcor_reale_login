package com.rmcor.reale.service;

import org.springframework.web.multipart.MultipartFile;

import com.rmcor.reale.utilites.ResponseModel;


public interface UploadService {

	public ResponseModel callUpload(MultipartFile file);

	public String createFile(MultipartFile file);

}
