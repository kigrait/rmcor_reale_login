FROM openjdk:8
ADD target/rmcor_reale_login.jar rmcor_reale_login.jar
EXPOSE 9100
ENTRYPOINT ["java", "-jar", "rmcor_reale_login.jar"]
